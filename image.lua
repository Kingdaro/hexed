-- quick and dirty (but convenient) centered image class
Image = Class{}

local folder = "/img/"

function Image:init(path)
	self.image = love.graphics.newImage(folder..path)
end

function Image:getWidth()
	return self.image:getWidth()
end

function Image:getHeight()
	return self.image:getHeight()
end

function Image:getSize()
	return self:getWidth(), self:getHeight()
end

function Image:draw(x, y, rot, sx, sy, ox, oy)
	love.graphics.draw(self.image, x, y, rot, sx, sy,
		self.image:getWidth()/2 + (ox or 0),
		self.image:getHeight()/2 + (oy or 0))
end
