MapBlock = Class {
	__includes = { Physics };
	isMapBlock = true;
}

function MapBlock:init(x, y, map, layer)
	Physics.init(self,
		x * map.tileWidth, y * map.tileHeight,
		map.tileWidth, map.tileHeight)
	self.map = map
	self.layer = layer
	self.mapx = x
	self.mapy = y
end

function MapBlock:destroy()
	self.layer:set(self.mapx, self.mapy, nil)
	self.map:forceRedraw()
	bump.remove(self)
end


Grass = Class {
	__includes = { MapBlock };
}

local grassImg = love.graphics.newImage("img/grass.png")
local grassQuad = love.graphics.newQuad(0, 0, 64, 64,
	grassImg:getWidth(), grassImg:getHeight())
local grassAnim = newAnimation(grassImg, 64, 64, 0.1, 0)

function Grass:init(...)
	MapBlock.init(self, ...)
	self.y = self.y + 32
	self.height = 32

	self.gravity = 0
	self.solid = false
	self.squish = Tween('outQuad', 0)
end

function Grass:collision(other, dx, dy)
	if other.isPlayer then
		self.squish:start(0.2, 0.8)
	end
end

function Grass:endCollision(other)
	if other.isPlayer then
		self.squish:start(0.2, 0)
	end
end

function Grass:draw()
	love.graphics.push()
	--love.graphics.scale(1, self.squish.value)
	if self.squish.value > 0 then
		love.graphics.drawq(grassImg, grassQuad,
			self.x, self.y - 32 + self.squish.value * 64,
			0, 1, 1 - self.squish.value)
	else
		grassAnim:draw(self.x, self.y + self.squish.value*self.height - 32)
	end
	love.graphics.pop()
end


Map = Class {}

function Map:init(folder)
	local map = maploader.load(folder..'/map.tmx')
	map.useSpriteBatch = true

	local x, y
	if map.properties.spawn then
		x, y = map.properties.spawn:match("(%d+)%s+(%d+)")
	end

	self.spawn = x and y
	and {
		x = x*map.tileWidth;
		y = y*map.tileHeight;
	}
	or {
		x = tonumber(x) or 200;
		y = tonumber(y) or 200;
	}

	self.layer = map "Level"

	self.background = {}
	self.foreground = {}

	local isBG = false
	for i,v in ipairs(map.layerOrder) do
		if v == self.layer then
			isBG = true
		else
			table.insert(isBG and self.background or self.foreground, v)
		end
	end

	for _,layer in pairs(map.layers) do
		if layer.class == "TileLayer" then
			for x, y, tile in layer:iterate() do
				local value = tile.properties.animation
				if value == "grass" then
					Gameplay:addToWorld(Grass(x, y, map, layer))
					layer:set(x, y, nil)
				end
			end
		end
	end

	map:draw()
	self.map = map
end

function Map:spawnPos()
	return self.spawn.x, self.spawn.y
end

function Map:createBlocks()
	local layer = self.map 'Collision' or self.map 'Level'
	if layer then
		if layer.class == "TileLayer" then
			for x, y, tile in layer:iterate() do
				local obj = MapBlock(x, y, self.map, layer)
				bump.addStatic(obj)
			end
		elseif layer.class == "ObjectLayer" then
			for i,v in ipairs(layer.objects) do
				local obj = Physics(v.x, v.y, v.width, v.height)
				bump.addStatic(obj)
				print(obj)
			end
		end
	else
		error('No "Collision" or "Level" layers found.', 0)
	end
end

function Map:size()
	return self.map.width * self.map.tileWidth, self.map.height * self.map.tileHeight
end

function Map:update(dt)
	grassAnim:update(dt)
end

function Map:drawLayer(layer)
	if layer and (layer.class ~= "ObjectLayer" or debugInfo) then
		local darkness = tonumber(layer.properties.darkness) or 0
		love.graphics.setColor(255 - darkness, 255 - darkness, 255 - darkness)
		layer:draw()
	end
end

function Map:draw()
	for i,v in pairs(self.background) do
		self:drawLayer(v)
	end
end

function Map:drawForeground()
	self:drawLayer(self.layer)
	for i,v in pairs(self.foreground) do
		self:drawLayer(v)
	end
end

function Map:drawRange(...)
	self.map:setDrawRange(...)
	self.map:draw()
end
