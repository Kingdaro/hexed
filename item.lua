Item = Class {
	__includes = { Physics };
	isItem = true;
}

function Item:init(x, y)
	Physics.init(self, x, y, 32, 64)
	self.solid = false
end

function Item:draw(x, y)
	if not (x and y) then
		x, y = self:center()
	end
	if self.image then
		love.graphics.setColor(255, 255, 255)
		self.image:draw(x, y - math.sin(gameTime*3)*5 - 25)
	else
		Physics.draw(self)
	end
end
