Title = {}

local logo = Image("title.png")
local clouds = CloudGroup()
local menu

function Title:enter()
	menu = Menu(nil, 250)
	menu:option("Game Start", function() Fade:switch(MapSelect) end)
	menu:option("Options")
	menu:option("Exit", function() love.event.quit() end)
end

function Title:update(dt)
	clouds:update(dt)
end

function Title:keypressed(k)
	if k == 'escape' then
		love.event.quit()
	end
end

function Title:mousepressed(x, y, button)
	menu:clicked(x, y)
end

function Title:draw()
	clouds:draw()

	local float = math.sin(gameTime * 2) * 5

	love.graphics.setColor(255, 255, 255)
	menu:draw(0, float)

	logo:draw(love.graphics.getWidth()/2, 140 + float)
end
