Tween = Class {}

function Tween:init(easing, value)
	self.easing = easing
	self.value = value
end

function Tween:start(time, value, callback, ...)
	self:stop()
	self.targetValue = value
	self.tween = tweenlib(time, self, { value = value }, self.easing, function(...)
		self.tween = nil
		self.targetValue = nil
		if callback then
			callback(...)
		end
	end, ...)
end

function Tween:stop(finish)
	if self.tween then
		tweenlib.stop(self.tween)
		self.tween = nil
		if finish then
			self.value = self.targetValue
			self.targetValue = nil
		end
	end
end
