ProjectileSpell = Class {
	__includes = { Physics };
	rot = 0;
	spin = 3;
	life = 1;
}

function ProjectileSpell:init(x, y, speed, dir, power)
	Physics.init(self, x, y)
	self.xvel = speed * dir
	self.power = power
	self.maxLife = self.life
	self.gravity = 0
	self.solid = false
end

function ProjectileSpell:update(dt)
	Physics.update(self, dt)
	self.life = self.life - dt
	if self.life < 0 then
		self:destroy()
	end
end

function ProjectileSpell:collision(other, dx, dy)
	if other.solid then
		self:destroy()
	end
end


FireSpell = Class {
	__includes = { ProjectileSpell };
}

function FireSpell:draw()
	love.graphics.setColor(255, 0, 0, 180)
	love.graphics.circle('fill', self.x, self.y, self.power)
end


EnergySpell = Class {
	__includes = { ProjectileSpell };
}

function EnergySpell:init(x, y, dir, speed, power)
	ProjectileSpell.init(self, x, y, dir, speed, power)

	local scale = 1 / 2^0.5
	self.x = x - power*scale
	self.y = y - power*scale
	self.width = power*scale*2
	self.height = self.width
end

function EnergySpell:collision(other, dx, dy)
	if other.isMapBlock then
		local sx, sy = self:center()
		local ox, oy = other:center()
		if math.dist(sx, sy, ox, oy) < self.width / 2 then
			other:destroy()
		end
	end
end

function EnergySpell:draw()
	love.graphics.setColor(200, 20, 255, 180*(self.life / self.maxLife))
	love.graphics.circle('fill', self.x, self.y, self.power)
end
