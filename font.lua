Font = Class{
	path = 'KOMIKAX_.ttf';
}

local folder = '/fonts/'

function Font:init(size, path)
	self.font = love.graphics.newFont(folder .. (path or self.path), size)
end

function Font:getWidth(text)
	return self.font:getWidth(text)
end

function Font:getHeight(text)
	return self.font:getHeight(text)
end

function Font:getSize(text)
	return self:getWidth(text), self:getHeight(text)
end

function Font:draw(...)
	love.graphics.setFont(self.font)
	love.graphics.print(...)
end

function Font:drawf(...)
	love.graphics.setFont(self.font)
	love.graphics.printf(...)
end


ShadowFont = Class {
	__includes = { Font };
	offset = 3;
}

function ShadowFont:draw(text, x, y)
	local r, g, b, a = love.graphics.getColor()

	love.graphics.setColor(0, 0, 0, 180 * (a/255))
	Font.draw(self, text, x + self.offset, y + self.offset)

	love.graphics.setColor(r, g, b, a)
	Font.draw(self, text, x, y)
end

function ShadowFont:drawf(text, x, y, ...)
	local r, g, b, a = love.graphics.getColor()

	love.graphics.setColor(0, 0, 0, 180 * (a/255))
	Font.drawf(self, text, x + self.offset, y + self.offset, ...)

	love.graphics.setColor(r, g, b, a)
	Font.drawf(self, text, x, y, ...)
end
