-- ability to walk and jump
Living = Class {
	__includes = { Physics };

	speed = 2000;
	xlimit = 400;
	dir = 1;
	moving = false;

	jumps = 0;
	jumpSpeed = 550;
	jumpBoost = 100;
	maxJumps = 2;
}

local jump1 = Sound("jump1.wav")
local jump2 = Sound("jump2.wav")

function Living:init()
	GameObject.init(self, 200, 200, 55, 55)
end

function Living:walk(dir)

	self.dir = dir < 0 and -1 or dir > 0 and 1 or self.dir
	self.moving = dir ~= 0
end

function Living:jump()
	if self.jumps > 0 then
		if self.jumps == 2 then
			jump1:play()
		elseif self.jumps == 1 then
			jump2:play()
		end

		self.yvel = -self.jumpSpeed
		self.y = self.y - 10
		self.jumps = self.jumps - 1
	end
end

function Living:collision(other, dx, dy)
	Physics.collision(self, other, dx, dy)
	if other.solid and dy < 0 then
		self.jumps = self.maxJumps
	end
end

function Living:update(dt)
	Physics.update(self, dt)
	if self.moving then
		self:accelerate(self.speed * self.dir * dt, 0)
	else
		self:decelerate(dt, true, false, 3)
	end
end


-- class for the main player thing
Player = Class {
	__includes = { Living };
	crouching = false;
	power = 0;
	powerSpeed = 1;
	powerLimit = 30;
	isPlayer = true;
}

local body = Image("player/body.png")
local eye = Image("player/eye.png")
local hand = Image("player/hand.png")
local stick = Image("stick.png")

function Player:init()
	Living.init(self)

	self.normalHeight = self.height
	self.normalSpeed = self.xlimit

	self.eyes = GameObject(self.x, self.y, 0, 0)
	self.hands = GameObject(self.x, self.y, 0, 0)

	self.turnAnim = Tween('outQuad', self.dir)
	self.crouchAnim = Tween('outQuad', 1)
	self.castAnim = Tween('outQuad', 0)

	self.inventory = Inventory()
	self.wand = nil
end

function Player:spawn()
end

function Player:walk(dir)
	if dir ~= self.dir and dir ~= 0 then
		self.turnAnim:start(0.3, dir < 0 and -1 or dir > 0 and 1)
	end
	Living.walk(self, dir)
end

function Player:crouch()
	self.crouchAnim:start(0.1, 0.2)
	self.crouching = true
end

function Player:stand()
	self.crouchAnim:start(0.1, 1, function()
		self.crouching = false
	end)
end

function Player:swing()
	self.castAnim:start(0.1, 1, function()
		self.castAnim:start(0.08, -0.7, function()
			self.castAnim:start(0.2, 0)
		end)
	end)
end

function Player:charge(dt)
	if self.power == 0 then
		self.castAnim:start(0.3, 1)
	end
	if self.wand then
		self.power = self.power - (self.power - self.powerLimit) * dt * self.powerSpeed
	end
end

function Player:fire()
	if self.power >= 15 or not self.wand then
		self.castAnim:start(0.08, -0.7, function()
			if self.wand then
				local x, y = self:center()
				self.xvel = 50 * -self.dir
				self.wand:cast(x, y, self.dir, self.power)
				self.power = 0
			end
			self.castAnim:start(0.2, 0)
		end)
	else
		self.castAnim:start(0.1, 0)
		self.power = 0
	end
end


function Player:collision(other, dx, dy)
	if other.isWand then
		Gameplay:removeFromWorld(other)
		self.wand = other
	else
		Living.collision(self, other, dx, dy)
	end
end

function Player:update(dt)
	Living.update(self, dt)

	local x, y = self:center()
	self.eyes:shiftTo(x, y + math.sin(gameTime * 5)*2, dt, 30)
	self.hands:shiftTo(x, y + 10 + math.sin(gameTime * 5)*5, dt, 15)
	self.eyes.x = x
	self.hands.x = x

	if self.crouching then
		self:accelerate(0, self.gravity*dt)
		self.xlimit = self.normalSpeed/3
	else
		self.xlimit = self.normalSpeed
	end
	self.height = self.crouchAnim.value * self.normalHeight
end

function Player:draw()
	local cx, cy = self:center()
	local ox, oy = cx - self.castAnim.value*8, cy
	local rot = self.x / self.width * math.sign(self.turnAnim.value)

	love.graphics.setColor(255, 255, 255)

	love.graphics.push()
		love.graphics.translate(ox, oy)
		love.graphics.scale(self.turnAnim.value, self.crouchAnim.value)
		love.graphics.rotate(rot)
		body:draw(0, 0)
	love.graphics.pop()

	local function applyTurn()
		love.graphics.translate(ox, oy)
		love.graphics.scale(self.turnAnim.value, 1)
		love.graphics.translate(-ox, -oy)
	end

	love.graphics.push()
		applyTurn()
		for i=-1, 1, 2 do
			eye:draw(
				self.eyes.x + 9*i + 10,
				self.eyes.y - 10)
		end
	love.graphics.pop()

	love.graphics.push()
		applyTurn()
		love.graphics.translate(self.hands.x + 13, self.hands.y)
		for i=-1, 1, 2 do
			local rot = (self.hands.y - 30 - self.y) / 80*i
			love.graphics.rotate(rot)
			if i == 1 then
				love.graphics.translate(0, -10)
				love.graphics.rotate(-self.castAnim.value*1.3)

				if self.wand then
					self.wand.image:draw(22*i + 10, -28)
				end
			end
			love.graphics.setColor(255, 255, 255)
			hand:draw(30*i, 0)
			if i == 1 then
				love.graphics.translate(0, 10)
			end
			love.graphics.rotate(-rot)
		end
	love.graphics.pop()

	if self.power > 0 then
		love.graphics.setColor(200, 50, 255, 180)
		love.graphics.circle('fill', cx + self.turnAnim.value * 70, cy, self.power)
	end
end

-- human-controlled player
Human = Class {
	__includes = Player;
	controls = {
		left = 'left';
		right = 'right';
		jump = 'up';
		crouch = 'down';
		cast = 'z';
	};
}

function Human:control(dt)
	local dir = 0
	if love.keyboard.isDown(self.controls.left) then
		dir = dir - 1
	end
	if love.keyboard.isDown(self.controls.right) then
		dir = dir + 1
	end
	self:walk(dir)

	if love.keyboard.isDown(self.controls.cast) and self.wand then
		self:charge(dt)
	end
end

function Human:keypressed(k)
	if k == self.controls.jump then
		self:jump()
	end
	if k == self.controls.crouch then
		self:crouch()
	end
	if k == self.controls.cast and not self.wand then
		self:swing()
	end
end

function Human:keyreleased(k)
	if k == self.controls.left or k == self.controls.right then
		self:walk(0)

	elseif k == self.controls.crouch then
		self:stand()

	elseif k == self.controls.cast and self.wand then
		self:fire()
	end
end
