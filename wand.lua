Wand = Class {
	__includes = { Item };
	isWand = true;
}

function Wand:cast(x, y, dir, power)
	if self.spell then
		Gameplay:addToWorld(self.spell(x + 50*dir, y, self.speed, dir, power))
	end
end

Stick = Class {
	__includes = { Wand };
	image = Image("stick.png");
	speed = 400;
	damage = 5;
	spell = FireSpell;
}
