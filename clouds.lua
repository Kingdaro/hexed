Cloud = Class {
	__includes = { GameObject };
	rot = 0;
}

local cloudImg = Image("cloud.png")

function Cloud:init(x, y, scale)
	GameObject.init(self, x, y)
	self.scale = scale
end

function Cloud:draw()
	local x, y = self:center()
	cloudImg:draw(x, y, self.rot, self.scale)
end

CloudGroup = Class {}

function CloudGroup:init()
	self.clouds = {}
	self.spawner = Timer.new()

	self.spawner:addPeriodic(0.5, function()
		self:addCloud()
	end)

	local cloud = self:addCloud()
	repeat
		self:update(0.1)
	until cloud.x <= 0
end

function CloudGroup:addCloud()
	local w,h = love.graphics.getMode()
	local cloud = Cloud(w + 100, math.random(0, h), math.random() * 0.75)
	table.insert(self.clouds, cloud)
	return cloud
end

function CloudGroup:update(dt)
	self.spawner:update(dt)

	for i=#self.clouds, 1, -1 do
		local v = self.clouds[i]
		if v.x > -100 then
			v.x = v.x - 100 * v.scale * dt
			v.rot = v.rot - 0.5*dt
		else
			table.remove(self.clouds, i)
		end
	end
end

function CloudGroup:draw(px, py)
	love.graphics.setColor(255, 255, 255, 120)
	for _, v in pairs(self.clouds) do
		v:draw()
	end
end
