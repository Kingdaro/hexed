MapSelect = {}

local menu

function MapSelect:enter()
	local fs = love.filesystem
	fs.mkdir('maps')
	local maps = fs.enumerate('maps')

	menu = Menu(nil, 50)

	for i,map in ipairs(maps) do
		menu:option(map, function()
			Gamestate.switch(Gameplay, map)
		end)
	end

	menu:option('')
	menu:option("Open Maps Folder", function()
		local dir = love.filesystem.getSaveDirectory()..'/maps'
		if love._os == "Windows" then
			os.execute("start "..dir)
		elseif love._os == "Linux" then
			os.execute("xdg-open "..dir)
		end
	end)
end

function MapSelect:keypressed(k)
	if k == 'escape' then
		Gamestate.switch(Title)
	end
end

function MapSelect:mousepressed(x, y)
	menu:clicked(x, y)
end

function MapSelect:draw(dt)
	menu:draw()
end
