DialogueText = Class {}

local font = ShadowFont(28)

function DialogueText:init(text, x, y, limit, time)
	self.text = text
	self.x = x
	self.y = y
	self.limit = limit
	self.tween = Tween('linear', 1)
	self.tween:start(time or #text / (1 / 0.05), #text, function()
		self:finished()
	end)
end

function DialogueText:update(dt)
end

function DialogueText:skip()
	self.char = #self.text
	self.tween:stop(true)
	self:finished()
end

-- callback for when the dialog fully types out
function DialogueText:finished() end

function DialogueText:draw()
	local w, h = love.graphics.getWidth(), 200
	love.graphics.setColor(255, 255, 255)
	font:drawf(self.text:sub(1, self.tween.value), self.x, self.y, self.limit, 'left')
end
