-- positioning and sizing
GameObject = Class {}

function GameObject:init(x, y, width, height)
	self.x = x or 0
	self.y = y or 0
	self.width = width or 0
	self.height = height or 0
end

function GameObject:centerx()
	return self.x + self.width/2
end

function GameObject:centery()
	return self.y + self.height/2
end

function GameObject:center()
	return self:centerx(), self:centery()
end

function GameObject:getBBox()
	return self.x, self.y, self.width, self.height
end

function GameObject:move(x, y)
	self.x = self.x + x
	self.y = self.y + y
end

function GameObject:goto(x, y)
	self.x = x
	self.y = y
end

 -- moves an object slowly using dt
function GameObject:shiftTo(x, y, dt, factor)
	self.x = self.x - (self.x - x) * dt * factor
	self.y = self.y - (self.y - y) * dt * factor
end

function GameObject:contains(x, y)
	return x >= self.x and x < self.x + self.width
	and y >= self.y and y < self.y + self.height
end

function GameObject:isTouching(other)
	local ax1, ay1 = self.x, self.y
	local ax2, ay2 = self.x + self.width, self.y + self.height
	local bx1, by1 = other.x, other.y
	local bx2, by2 = other.x + other.width, other.y + other.height
	return ax1 < bx2 and ax2 > bx1 and ay1 < by2 and ay2 > by1
end

function GameObject:draw()
	love.graphics.rectangle('fill', self.x, self.y, self.width, self.height)
end

-- for velocity-based movement
Velocity = Class {
	__includes = { GameObject };
	xvel = 0;
	yvel = 0;
	xlimit = math.huge;
	ylimit = 1000;
	gravity = 1400;
	frozen = false;
}

local function clamp(min, n, max)
	return n < min and min or n > max and max or n
end

function Velocity:accelerate(x, y)
	self.xvel = self.xvel + x
	self.yvel = self.yvel + y
end

function Velocity:decelerate(dt, x, y, friction)
	if x then
		self.xvel = self.xvel - self.xvel * dt * friction
	end
	if y then
		self.yvel = self.yvel - self.yvel * dt * friction
	end
end

function Velocity:update(dt)
	if self.frozen then return end
	self.xvel = clamp(-self.xlimit, self.xvel, self.xlimit)
	self.yvel = self.yvel + self.gravity * dt --clamp(-self.ylimit, self.yvel + self.gravity * dt, self.ylimit)
	self.x = self.x + self.xvel * dt
	self.y = self.y + self.yvel * dt
end
