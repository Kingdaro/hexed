-- collision detection and resolution
Physics = Class {
	__includes = { Velocity };
	solid = true;
}

function Physics:collision(other, dx, dy)
	if other.solid then
		self:resolve(dx, dy)
	end
end

function Physics:resolve(dx, dy)
	if (self.xvel > 0 and dx < 0)
	or (self.xvel < 0 and dx > 0) then
		self.x = self.x + dx
		self.xvel = 0
	end
	if (self.yvel > 0 and dy < 0)
	or (self.yvel < 0 and dy > 0) then
		self.y = self.y + dy
		self.yvel = 0
	end
end

function Physics:endCollision(other)
end

function Physics:destroy()
	if self.world then
		self.world:remove(self)
	end
end


World = Class {}

function World:init()
	self.objects = {}
	self.callbacks = {
		update = function() end;
		draw = function() end;
	}
end

function World:clear()
	for _,v in pairs(self.objects) do
		bump.remove(v)
	end
	self.objects = {}
end

function World:add(obj, type)
	if type == nil or type == 'dynamic' then
		bump.add(obj)
	elseif type == 'static' then
		bump.addStatic(obj)
	end
	table.insert(self.objects, obj)
	obj.world = self
end

function World:remove(obj)
	for i=#self.objects, 1, -1 do
		local v = self.objects[i]
		if v == obj then
			bump.remove(v)
			table.remove(self.objects, i)
		end
	end
end

function World:update(dt)
	for _,v in pairs(self.objects) do
		v:update(dt)
	end
end

function World:draw()
	for _,v in pairs(self.objects) do
		v:draw()
	end
end
