function love.load()
	love.graphics.setDefaultImageFilter('linear','nearest')
	love.graphics.setBackgroundColor(138, 212, 246)

	Class = require 'hump.class'
	Camera = require 'hump.camera'
	Gamestate = require 'hump.gamestate'
	Timer = require 'hump.timer'
	maploader = require 'ATL.Loader'
	bump = require 'bump'
	tweenlib = require 'tween'

	require 'AnAL'
	-- require 'richtext'
	require 'extra'

	require 'image'
	require 'sound'
	require 'font'
	require 'menu'
	require 'tweenobj'
	require 'fade'
	require 'dialogue'
	require 'components'
	require 'world'
	require 'map'
	require 'clouds'
	require 'spells'
	require 'item'
	require 'wand'
	require 'inventory'
	require 'player'

	require 'title'
	require 'mapselect'
	require 'gameplay'

	Gamestate.registerEvents()
	Gamestate.switch(Title)
	
	gameTime = 0
	debugInfo = false
	fpsFont = ShadowFont(30)

	Fade:init()
end

function love.update(dt)
	if dt > 1/30 then return end
	gameTime = gameTime + dt
	tweenlib.update(dt)
	Timer.update(dt)
end

function love.keypressed(k)
	if k == 'f1' then
		debugInfo = not debugInfo
	end
end

function love.draw()
	Fade:draw()

	if debugInfo then
		love.graphics.setColor(255, 255, 255)
		fpsFont:draw('FPS: '..love.timer.getFPS(), 15, 0)
	end
end
