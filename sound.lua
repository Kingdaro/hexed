Sound = Class {}

local folder = "/sound/"
local stackSize = 5

function Sound:init(path)
	self.stack = {}
	for i=1, stackSize do
		self.stack[i] = love.audio.newSource(folder..path)
	end
end

function Sound:play()
	self.stack[1]:stop()
	self.stack[1]:play()
	table.insert(self.stack, table.remove(self.stack, 1))
end
