function love.conf(t)
	t.title = "Hexed"
	t.author = "Darius Tall, Jon Fazio, Taylor Wick"
	t.url = nil
	t.identity = 'hexed-data'
	t.version = "0.8.0"
	t.screen.fullscreen = false
	t.screen.vsync = false
	t.screen.fsaa = 0
end
