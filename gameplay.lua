Gameplay = {}

local world, map, hero, camera, clouds
local dialogue

function Gameplay:init()
	function bump.collision(a, b, dx, dy)
		a:collision(b, dx, dy)
		b:collision(a,-dx,-dy)
	end
	function bump.endCollision(a, b)
		a:endCollision(b)
		b:endCollision(a)
	end
end

function Gameplay:enter(prev, mapPath)
	maploader.path = "maps/"

	bump.initialize()

	world = World()

	map = Map(mapPath)
	hero = Human()
	camera = Camera()
	clouds = CloudGroup()

	self:addToWorld(hero)
	map:createBlocks(4)

	dialogue = DialogueText(
		"I always wanted to be somebody, but now I realize I should have been more specific.",
		20, 20, 760)
end

function Gameplay:update(dt)
	local panSpeed = 3
	camera:move(
		-(camera.x - hero.x) * dt * panSpeed,
		-(camera.y - hero.y) * dt * panSpeed)

	local w,h = love.graphics.getMode()
	local mw, mh = map:size()
	camera:lookAt(
		math.clamp(w/2, camera.x, mw - w/2),
		math.clamp(h/2, camera.y, mh - h/2))

	map:drawRange(camera.x - w/2, camera.y - h/2, w, h)

	if hero.y > mh + 200 then
		hero:goto(map:spawnPos())
	end

	world:update(dt)
	hero:control(dt)
	clouds:update(dt)
	map:update(dt)

	--dialogue:update(dt)

	bump.collide()
end

function Gameplay:keypressed(k)
	if k == 'escape' then
		Gamestate.switch(MapSelect)
	end
	hero:keypressed(k)

	if k == 'f2' then
		Gameplay:addToWorld(Stick(hero.x, hero.y - 200))
	end
end

function Gameplay:keyreleased(k)
	hero:keyreleased(k)
end

function Gameplay:getPlayer()
	return hero
end

function Gameplay:addToWorld(obj, static)
	world:add(obj, static)
end

function Gameplay:removeFromWorld(...)
	world:remove(...)
end

function Gameplay:draw()
	local w,h = love.graphics.getMode()

	clouds:draw()

	camera:attach()
	--map:draw()

	map:draw()
	world:draw()
	map:drawForeground()

	camera:detach()

	--dialogue:draw()
end
