Fade = Class {
	__includes = { Tween };
}

function Fade:init()
	Tween.init(self, 'linear', 0)
end

function Fade:switch(state, time)
	if not state then
		error("Missing state to switch to", 2)
	end

	time = time or 1
	self:start(time/2, 255, function()
		Gamestate.switch(state)
		self:start(time/2, 0)
	end)
end

function Fade:draw()
	local w,h = love.graphics.getMode()
	love.graphics.setColor(0, 0, 0, self.value)
	love.graphics.rectangle('fill', 0, 0, w, h)
end
