Menu = Class {
	x = 0;
	y = 0;
}

local menuFont = ShadowFont(44)

function Menu:init(x, y)
	self.x = x or love.graphics.getWidth() / 2
	self.y = y or love.graphics.getHeight() / 2
	self.options = {}
end

function Menu:option(name, callback)
	local option = GameObject()
	option.name = name
	option.callback = callback or function() end

	table.insert(self.options, option)
	self:realign()
end

function Menu:realign(x, y)
	x = x or self.x
	y = y or self.y
	local curHeight = y
	for i,option in ipairs(self.options) do
		local width, height = menuFont:getSize(option.name)
		option.x = x - width/2
		option.y = curHeight
		option.width = width
		option.height = height

		curHeight = curHeight + height
	end
end

function Menu:clicked(x, y)
	for k,v in pairs(self.options) do
		if v:contains(x, y) then
			v.callback()
		end
	end
end

function Menu:draw(ox, oy)
	ox = ox or 0
	oy = oy or 0
	love.graphics.setColor(255, 255, 255)
	for _,option in pairs(self.options) do
		local x, y = option:getBBox()
		menuFont:draw(option.name, x + ox, y + oy)
	end
end
